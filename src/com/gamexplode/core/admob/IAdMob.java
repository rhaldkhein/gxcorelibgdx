package com.gamexplode.core.admob;

import java.util.Date;

import com.gamexplode.core.IBase;

public interface IAdMob extends IBase {
	public abstract void setGender(String gender);
	public abstract void setBirthday(Date birthday);
	public abstract void showInterstitial();
	public abstract void showBanner();
	public abstract void loadAds();
	public abstract void removeAds();
}
