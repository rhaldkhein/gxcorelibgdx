package com.gamexplode.core.math;

import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class LinearPath<T extends Vector<T>> implements Path<T> {

	/**
	 * Simple linear interpolation
	 * 
	 * @param out The {@link Vector} to set to the result.
	 * @param t The location (ranging 0..1) on the line.
	 * @param p0 The start point.
	 * @param p1 The end point.
	 * @param tmp A temporary vector to be used by the calculation.
	 * @return The value specified by out for chaining
	 */
	public static <T extends Vector<T>> T linear(final T out, final float t, final T p0, final T p1, final T tmp) {
		// B1(t) = p0 + (p1-p0)*t
		return out.set(p0).scl(1f - t).add(tmp.set(p1).scl(t)); // Could just use lerp...
	}

	public Array<T> points = new Array<T>();
	private T tmp;
	private T tmp2;
	private T tmp3;

	public LinearPath() {
		// Instance with late initialization
	}

	@SafeVarargs
	public LinearPath(final T... points) {
		
	}

	public LinearPath(final T[] points, final int offset, final int length) {
		// set(points, offset, length);
	}

	public LinearPath(final Array<T> points, final int offset, final int length) {
		// set(points, offset, length);
	}

	@Override
	public T derivativeAt(T out, float t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T valueAt(T out, float t) {
		// TODO Auto-generated method stub
		linear(out, t, points.get(0), points.get(1), tmp);
		return out;
	}

	@Override
	public float approximate(T v) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float locate(T v) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float approxLength(int samples) {
		// TODO Auto-generated method stub
		return 0;
	}

}
