package com.gamexplode.core;

public abstract class AssetFontGenerator {
	
	public abstract void generate();
	
}
