package com.gamexplode.core.scenebox;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public abstract class ActorBody extends Actor {

	/**
	 * Active
	 */
	protected boolean a; // active
	protected float x; // x
	protected float y; // y
	protected float w; // width
	protected float h; // height
	protected float sx; // scale x
	protected float sy; // scale y
	protected float r; // rotation
	protected float hw; // half width, also origin
	protected float hh; // half height, also origin
	protected float dx; // draw x
	protected float dy; // draw y

	protected Body body;
	private Color color;
	private boolean mLog;

	/**
	 * Method to call to define body.
	 * 
	 * @return
	 */
	protected abstract BodyDef definition();

	/**
	 * Method to call to construct body fixtures.
	 * 
	 * @param body
	 */
	protected abstract void fixture(Body body);

	/**
	 * Method to call when this actor is created.
	 */
	protected abstract void create();

	/**
	 * Method to call when this actor is attached to any parent.
	 */
	protected abstract void attach();

	public ActorBody() {
		mLog = false;
	}

	@Override
	protected void setParent(Group parent) {
		super.setParent(parent);
		attach();
	}

	/**
	 * Create a body for this actor and add to specified world.
	 * 
	 * @param world
	 */
	public void addToWorld(World world) {
		// Create body
		if (body == null) {
			BodyDef def = definition();
			if (def == null) {
				def = new BodyDef();
				def.type = BodyType.KinematicBody;
			}
			body = world.createBody(def);
			fixture(body);
			create();
		}
		if (!body.isActive()) {
			body.setActive(true);
		}
		a = body.isActive();
	}

	/**
	 * Return the body of this actor.
	 * 
	 * @return Body
	 */
	public Body getBody() {
		return body;
	}

	/**
	 * Get the stage world.
	 */
	@Override
	public StageWorld getStage() {
		return (StageWorld) super.getStage();
	}

	public void setActive(boolean flag) {
		body.setActive(flag);
		a = body.isActive();
	}

	/**
	 * Sets the position of actor's body.
	 */
	@Override
	public void setPosition(float x, float y) {
		setPosition(x, y, body.getAngle());
	}

	@Override
	public void setPosition(float x, float y, int angle) {
		setPosition(x, y, (float) angle);
	}

	public void setPosition(float x, float y, float angle) {
		body.setTransform(x, y, angle);
		// super.setPosition(x, y);
	}

	public void setPosition(Vector2 pos) {
		body.setTransform(pos, body.getAngle());
	}

	public void setPosition(Vector2 pos, float angle) {
		body.setTransform(pos, angle);
	}

	/**
	 * Set the rotation of the body. (Not the actor)
	 * 
	 * @param degrees
	 */
	@Override
	public void setRotation(float degrees) {
		// No need to set rotation for scene2d actor.
		// super.setRotation(degrees);
		body.setTransform(body.getPosition(), (float) Math.toRadians(degrees));
	}

	/**
	 * Get the x position.
	 */
	@Override
	public float getX() {
		return x;
	}

	/**
	 * Get the y position.
	 */
	@Override
	public float getY() {
		return y;
	}

	/**
	 * Get the vector2 position.
	 */
	public Vector2 getPosition() {
		return body.getPosition();
	}

	@Override
	public float getRotation() {
		return r;
	}

	@Override
	public float getOriginX() {
		return hw;
	}

	@Override
	public float getOriginY() {
		return hh;
	}

	@Override
	protected void sizeChanged() {
		super.sizeChanged();
		w = getWidth();
		h = getHeight();
		hw = w / 2;
		hh = h / 2;
	}
	
	/**
	 * Update the rectangle for drawing.
	 */
	protected void updateRectangle() {
		x = body.getPosition().x;
		y = body.getPosition().y;
		dx = x - hw;
		dy = y - hh;
		sx = getScaleX();
		sy = getScaleY();
		r = (float) Math.toDegrees(body.getAngle());
		// No need to set origin, as it depend on body
		// super.setOrigin(hw, hh);
		// No need to set position, as it depend on body
		// super.setPosition(x, y);
		// No need to set rotation, as it depend on body
		// super.setRotation(r);
		if (mLog) {
			Gdx.app.log("ActorBody", "" + this.toString() + "\t | x " + x + " - y " + y + "\t | w " + w + " - h " + h + "\t | hw " + hw + " - hh "
					+ hh + "\t | dx " + dx + " - dy " + dy + "\t | sx " + sx + " - sy " + sy + " - r " + r);
		}
	}

	@Override
	public void act(float delta) {
		super.act(delta);
		updateRectangle();
	}

	/**
	 * Draw the actor. For this base class, deal only with alpha and let children handle the rest.
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
	}

	/**
	 * Remove or detach the actor from stage but does not destroy it. This also deactivates the actor's body.
	 * 
	 * @return Boolean True if successful
	 */
	@Override
	public boolean remove() {
		if (super.remove()) {
			// Just sleep, do not destroy it, because it might be added again later.
			if (body.isActive()) {
				body.setActive(false);
				a = body.isActive();
			}
			return true;
		}
		return false;
	}

	/**
	 * Remove and destroy the actor and its body.
	 * 
	 * @return
	 */
	public boolean destroy() {
		if (remove()) {
			body.getWorld().destroyBody(body);
			return true;
		}
		return false;
	}

	/**
	 * Destroy all fixtures attached to the body.
	 */
	public void destroyFixtures() {
		for (Fixture fixture : body.getFixtureList()) {
			body.destroyFixture(fixture);
		}
	}

	public void clearVelocity() {
		body.setLinearVelocity(0, 0);
		body.setAngularVelocity(0);
	}

	public void setLog(boolean flag) {
		mLog = flag;
	}

}
