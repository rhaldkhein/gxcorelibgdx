package com.gamexplode.core.scenebox;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.gamexplode.core.Config;

public class ImageActorBody extends ActorBody {

	private TextureRegion mTexture;

	@Override
	protected BodyDef definition() {
		return null;
	}

	@Override
	protected void fixture(Body body) {
	}

	@Override
	protected void create() {
	}

	@Override
	protected void attach() {
	}

	/**
	 * Set the size of actor based on current image texture.
	 */
	public void setSizeFromCurrentTexture() {
		if (mTexture == null) {
			throw new GdxRuntimeException("Unable to set size from image. No currrent texture or animation set.");
		}
		setSizeFromTexture(mTexture);
	}

	/**
	 * Set the size of actor based on specified image texture.
	 */
	public void setSizeFromTexture(TextureRegion tex) {
		super.setSize((float) tex.getRegionWidth() / Config.PPU, (float) tex.getRegionHeight() / Config.PPU);
	}

	/**
	 * Set current texture to render.
	 * 
	 * @param animation {@link Animation}
	 */
	public void setTexture(TextureRegion texture) {
		mTexture = texture;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (!a)
			return;
		super.draw(batch, parentAlpha);
		// Gdx.app.log("ImageActorBody", "mTexture " + mTexture);
		if (mTexture != null) {
			batch.draw(mTexture, dx, dy, x, y, w, h, sx, sy, r);
		}
	}

}
