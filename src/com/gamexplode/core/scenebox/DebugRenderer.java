package com.gamexplode.core.scenebox;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class DebugRenderer extends ShapeRenderer {

	private Box2DDebugRenderer mWorldDebugRenderer;

	public DebugRenderer() {
		super();
		create();
	}

	public DebugRenderer(int maxVertices) {
		super(maxVertices);
		create();
	}

	public DebugRenderer(int maxVertices, ShaderProgram defaultShader) {
		super(maxVertices, defaultShader);
		create();
	}

	private void create() {
		mWorldDebugRenderer = new Box2DDebugRenderer();
	}

	public void renderWorld(World world) {
		mWorldDebugRenderer.render(world, getProjectionMatrix());
	}

}
