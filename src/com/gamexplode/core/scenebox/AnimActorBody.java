package com.gamexplode.core.scenebox;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

public class AnimActorBody extends ImageActorBody {

	public static float DEFAULT_ANIM_DURATION = 0.025f;
	public static PlayMode DEFAULT_ANIM_MODE = Animation.PlayMode.LOOP;

	private ObjectMap<String, Animation> mAnimationMap;
	private Animation mAnimation;
	private float mAnimStateTime;

	public AnimActorBody() {
		super();
	}

	@Override
	public void setSizeFromCurrentTexture() {
		// Do not call super method, as it will use the non animated image.
		// super.setSizeFromCurrentTexture();
		setSizeFromTexture(mAnimation.getKeyFrame(mAnimStateTime));
	}

	/**
	 * Add animation with default animation frame duration and play mode.
	 * 
	 * @param name
	 * @param texture
	 * @param cols
	 * @param rows
	 */
	public void addAnimation(String name, TextureRegion texture, int cols, int rows) {
		addAnimation(name, texture, cols, rows, DEFAULT_ANIM_DURATION, DEFAULT_ANIM_MODE);
	}

	/**
	 * Add animation with default play mode.
	 * 
	 * @param name
	 * @param texture
	 * @param cols
	 * @param rows
	 * @param duration
	 */
	public void addAnimation(String name, TextureRegion texture, int cols, int rows, float duration) {
		addAnimation(name, texture, cols, rows, duration, DEFAULT_ANIM_MODE);
	}

	/**
	 * Add animation by providing the raw requirements.
	 * 
	 * @param name
	 * @param texture
	 * @param cols
	 * @param rows
	 * @param duration
	 * @param mode
	 */
	public void addAnimation(String name, TextureRegion texture, int cols, int rows, float duration, Animation.PlayMode mode) {
		TextureRegion[][] tmp = texture.split(texture.getRegionWidth() / cols, texture.getRegionHeight() / rows);
		TextureRegion[] frames = new TextureRegion[cols * rows];
		int index = 0, i, j;
		for (i = 0; i < rows; i++) {
			for (j = 0; j < cols; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		Animation anim = new Animation(duration, frames);
		anim.setPlayMode(mode);
		addAnimation(name, anim);
	}

	/**
	 * Add animation for this actor.
	 * 
	 * @param name
	 * @param animation
	 */
	public void addAnimation(String name, Animation animation) {
		if (mAnimationMap == null) {
			mAnimationMap = new ObjectMap<String, Animation>();
		}
		mAnimationMap.put(name, animation);
	}

	/**
	 * Set / change animation object.
	 * 
	 * @param animation {@link Animation}
	 * @param loop
	 */
	public void setAnimation(String name) {
		if (!mAnimationMap.containsKey(name)) {
			throw new GdxRuntimeException("Unable to set animation named " + name + ". It does not exist.");
		}
		mAnimation = mAnimationMap.get(name);
		mAnimStateTime = 0f;
	}

	/**
	 * Get animation by name.
	 * 
	 * @param name
	 * @return {@link Animation}
	 */
	public Animation getAnimation(String name) {
		return mAnimationMap.get(name);
	}

	/**
	 * Get current animation
	 * 
	 * @return {@link Animation}
	 */
	public Animation getAnimation() {
		return mAnimation;
	}

	/**
	 * Remove all animations
	 */
	public void clearAnimations() {
		mAnimationMap.clear();
	}

	@Override
	public void act(float delta) {
		if (mAnimation != null) {
			mAnimStateTime += delta;
			setTexture(mAnimation.getKeyFrame(mAnimStateTime));
		}
		super.act(delta);
	}

}
