package com.gamexplode.core.scenebox;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gamexplode.core.Config;

public class StageWorld extends Stage {

	private static Vector2 DEF_GRAVITY = new Vector2(0, -10);
	private static float DEF_TIMESTEP = 1 / 300f;
	private static int DEF_VEL_ITER = 6;
	private static int DEF_POS_ITER = 2;

	private World mWorld;
	private float mWorldAccumulator;
	private float mWorldTimeStep;
	private int mWorldVelocityIterations;
	private int mWorldPositionIterations;
	private boolean mAct;

	private boolean mDebug;
	private DebugRenderer mWorldDebugRenderer;

	public StageWorld() {
		this(DEF_GRAVITY, DEF_TIMESTEP, DEF_VEL_ITER, DEF_POS_ITER);
	}

	public StageWorld(Viewport viewport) {
		this(viewport, DEF_GRAVITY, DEF_TIMESTEP, DEF_VEL_ITER, DEF_POS_ITER);
	}

	public StageWorld(Viewport viewport, Batch batch) {
		this(viewport, batch, DEF_GRAVITY, DEF_TIMESTEP, DEF_VEL_ITER, DEF_POS_ITER);
	}

	public StageWorld(Viewport viewport, Vector2 gravity) {
		this(viewport, gravity, DEF_TIMESTEP, DEF_VEL_ITER, DEF_POS_ITER);
	}

	public StageWorld(Viewport viewport, Batch batch, Vector2 gravity) {
		this(viewport, batch, gravity, DEF_TIMESTEP, DEF_VEL_ITER, DEF_POS_ITER);
	}

	public StageWorld(Viewport viewport, float timeStep, int velocityIterations, int positionIterations) {
		this(viewport, DEF_GRAVITY, timeStep, velocityIterations, positionIterations);
	}

	public StageWorld(Vector2 gravity, float timeStep, int velocityIterations, int positionIterations) {
		super();
		create(gravity, timeStep, velocityIterations, positionIterations);
	}

	public StageWorld(Viewport viewport, Vector2 gravity, float timeStep, int velocityIterations, int positionIterations) {
		super(viewport);
		create(gravity, timeStep, velocityIterations, positionIterations);
	}

	public StageWorld(Viewport viewport, Batch batch, Vector2 gravity, float timeStep, int velocityIterations, int positionIterations) {
		super(viewport, batch);
		create(gravity, timeStep, velocityIterations, positionIterations);
	}

	/**
	 * Construct the world and its time.
	 * 
	 * @param gravity Gravity of the world.
	 * @param timeStep Time step of the world.
	 * @param velocityIterations Velocity iterations of the world.
	 * @param positionIterations Position iterations of the world.
	 */
	private void create(Vector2 gravity, float timeStep, int velocityIterations, int positionIterations) {
		// Create world
		mWorld = new World(gravity, true);
		mWorldTimeStep = timeStep;
		mWorldVelocityIterations = velocityIterations;
		mWorldPositionIterations = positionIterations;
		mWorldAccumulator = 0f;
		mAct = true;
		mDebug = false;

	}

	/**
	 * Set debug flag.
	 * 
	 * @param flag Boolean.
	 */
	public void setDebugWorld(boolean flag) {
		// Make sure mode is development.
		if (!Config.DEVEL) {
			mDebug = false;
			return;
		}
		mDebug = flag;
		if (mWorldDebugRenderer == null && mDebug) {
			mWorldDebugRenderer = new DebugRenderer();
			mWorldDebugRenderer.setProjectionMatrix(getCamera().combined);
		}
	}

	public DebugRenderer getDebugRenderer() {
		return mWorldDebugRenderer;
	}

	/**
	 * DEPRECATED: This method is not allowed. Instead, Use the method {@link #addActor(ActorBody)}.
	 */
	@Override
	@Deprecated
	public void addActor(Actor actor) {
		throw new GdxRuntimeException("Adding instance of Actor is not allowed. Instead, add instance of ActorBody");
	}

	/**
	 * Add instance of ActorBody.
	 * 
	 * @param actor ActorBody
	 */
	public void addActor(ActorBody actor) {
		super.addActor(actor);
		actor.addToWorld(mWorld);
	}

	/**
	 * Return the world.
	 * 
	 * @return World
	 */
	public World getWorld() {
		return mWorld;
	}

	/**
	 * Pause the world. No action & movement is shown but drawing is enabled.
	 * 
	 * @param flag Boolean
	 */
	public void setPause(boolean flag) {
		mAct = !flag;
	}

	/**
	 * Loop act and step with default delta.
	 */
	@Override
	public void act() {
		act(Gdx.graphics.getDeltaTime());
	}

	/**
	 * Loop act and step with specific delta.
	 */
	@Override
	public void act(float delta) {
		if (mAct) {
			// Using fixed time step
			mWorldAccumulator += delta;
			while (mWorldAccumulator >= delta) {
				mWorld.step(mWorldTimeStep, mWorldVelocityIterations, mWorldPositionIterations);
				mWorldAccumulator -= mWorldTimeStep;
			}
			// Act after world step
			super.act(delta);
		}
		// Draw the world
		super.draw();
		if (mDebug) {
			mWorldDebugRenderer.renderWorld(mWorld);
		}
	}

	/**
	 * DEPRECATED: This method is not allowed. Because is done automatically inside {@link #act()} method.
	 */
	@Override
	@Deprecated
	public void draw() {
		throw new GdxRuntimeException("StageWorld's draw method is not allowed. Because is done automatically in act method.");
	}

	@Override
	public void dispose() {
		super.dispose();
		mWorld.dispose();
	}

}
