package com.gamexplode.core.audio;

import com.badlogic.gdx.audio.Sound;
import com.gamexplode.core.AssetManager;
import com.gamexplode.core.Core;
import com.gamexplode.core.Environment;
import com.gamexplode.core.Settings;
import com.gamexplode.core.TextureManager;

/**
 * SoundManager is a singleton that manages the use of sounds.
 * 
 * This class depends on Core's AssetManager member. Before you use atlas, make sure it is loaded in asset manager. Dispose is handled by asset
 * manager.
 * 
 * @author Kevin
 */
public class SoundManager {

	private static SoundManager INSTANCE;

	private AssetManager assetManager;
	private String currentDir;
	private String baseDir;
	private String fileExt;
	private boolean enabled;

	public SoundManager() {
		baseDir = Environment.BASE_DIR;
		assetManager = AssetManager.i();
		useDefault();
		syncSettings();
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * 
	 * @return {@link TextureManager} object.
	 */
	public static SoundManager getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * 
	 * @return {@link TextureManager} object.
	 */
	public static SoundManager i() {
		if (INSTANCE == null) {
			INSTANCE = new SoundManager();
		}
		return INSTANCE;
	}

	/**
	 * Uses the default settings.
	 */
	public void useDefault() {
		currentDir = "";
		fileExt = ".mp3";
		enabled = true;
	}
	
	/**
	 * Sync enable flag to settings.
	 */
	private void syncSettings() {
		// Get the default settings space.
		Settings settings = Core.i().getSettings();
		if (!settings.contains("sound")) {
			settings.setBoolean("sound", true);
		}
		enabled = settings.getBoolean("sound");
	}

	/**
	 * Set & use specified directory.
	 * 
	 * @param pathDir
	 */
	public void useDirectory(String pathDir) {
		if(!pathDir.endsWith("/")){
			pathDir += "/";
		}
		currentDir = pathDir;
	}

	/**
	 * Enable or disable sound.
	 * 
	 * @param enable
	 */
	public void setEnabled(boolean enable) {
		enabled = enable;
		syncSettings();
	}

	/**
	 * Get a sound from asset manager.
	 * 
	 * @param name
	 * @return
	 */
	public Sound getSound(String name) {
		return assetManager.get(baseDir + currentDir + name + fileExt, Sound.class);
	}

	/**
	 * Check sound flag.
	 * 
	 * @return
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Play the specified sound.
	 * 
	 * @param name
	 */
	public void playSound(String name) {
		if (enabled) {
			getSound(name).play();
		}
	}

	/**
	 * Play the specified sound with volume.
	 * 
	 * @param name
	 * @param volume
	 */
	public void playSound(String name, float volume) {
		if (enabled) {
			getSound(name).play(volume);
		}
	}

	/**
	 * Play the specified sound with all options.
	 * 
	 * @param name
	 * @param volume
	 * @param pitch
	 * @param pan
	 */
	public void playSound(String name, float volume, float pitch, float pan) {
		if (enabled) {
			getSound(name).play(volume, pitch, pan);
		}
	}

	/**
	 * Toggle set enable flag.
	 */
	public void toggleEnabled() {
		setEnabled(!isEnabled());
	}
	
	/**
	 * Set extension for audio files.
	 * 
	 * @param ext
	 */
	public void setExtension(String ext) {
		fileExt = ext;
	}

}
