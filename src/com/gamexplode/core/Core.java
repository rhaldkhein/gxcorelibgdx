package com.gamexplode.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.gamexplode.core.audio.SoundManager;
import com.gamexplode.core.io.FileFinder;
import com.gamexplode.core.screen.ScreenManager;

public class Core {

	private static Core instance;

	// Required members, instantiated on initialization.
	private AssetManager assetManager;
	private SoundManager soundManager;
	private ObjectMap<String, Settings> mapSettings;
	private ObjectMap<String, IBase> mapInterfaces;

	// Optional members, instantiated on get method call.
	private HttpClient httpClient;

	// Other members.
	private boolean initialized;

	/**
	 * Singleton constructor because of protected.
	 */
	protected Core() {
		initialized = false;
		_initialize();
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * 
	 * @return {@link Core} object.
	 */
	public static Core getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * 
	 * @return {@link Core} object.
	 */
	public static Core i() {
		if (instance == null) {
			instance = new Core();
		}
		return instance;
	}

	/**
	 * Internal initialization of members.
	 */
	private void _initialize() {
		// Map collection containers.
		mapInterfaces = new ObjectMap<String, IBase>();
		mapSettings = new ObjectMap<String, Settings>();
		// Other members.
		// assetManager = new AssetManager();
		assetManager = AssetManager.i();
	}

	/**
	 * Starts the core engine. Enqueue necessary resource.
	 */
	public void initialize() {
		// Only single initialization.
		if (initialized)
			return;
		// Enable file logging on development.
		if (Config.DEVEL) {
			// LOG = new Logger("log_" + Config.GAME_NAME.toLowerCase() + ".txt");
		}
		// Open load screen textures.
		if (Config.LOADSCREEN_ENABLED) {
			assetManager.load(Config.LOADSCREEN_ATLAS, TextureAtlas.class);
			assetManager.finishLoading();
		}

		// Setup all global assets for loading.
		assetManager.loadSetup(new AssetLoadSetup() {
			@Override
			public void setup() {

				FileFinder finder = new FileFinder();
				finder.setRecursive(true);

				// Global Textures
				finder.addPath(Gdx.files.internal(Environment.BASE_DIR + Config.TEXTURES_DIRECTORY));
				finder.setFilterSuffix(".png");
				for (FileHandle file : finder.find()) {
					if (Config.DEVEL)
						Gdx.app.log(ConfigCore.TAG, "Queue | Texture | " + file.path());
					assetManager.load(file.path(), Texture.class);
				}

				// Global Atlas
				finder.reset(true);
				finder.addPath(Gdx.files.internal(Environment.BASE_DIR + Config.ATLAS_DIRECTORY));
				finder.setFilterSuffix(".atlas");
				for (FileHandle file : finder.find()) {
					if (Config.DEVEL)
						Gdx.app.log(ConfigCore.TAG, "Queue | Atlas | " + file.path());
					assetManager.load(file.path(), TextureAtlas.class);
				}

				// Global Particle Effects
				finder.reset(true);
				finder.addPath(Gdx.files.internal(Environment.BASE_DIR + Config.PARTICLES_DIRECTORY));
				finder.setFilterSuffix(".particle");
				for (FileHandle file : finder.find()) {
					if (Config.DEVEL)
						Gdx.app.log(ConfigCore.TAG, "Queue | Particle | " + file.path());
					assetManager.load(file.path(), ParticleEffect.class);
				}

				// Global Fonts
				AssetFontGenerator generator = assetManager.getFontGenerator();
				if (generator != null) {
					generator.generate();
				}

				// Global Sounds
				finder.reset(true);
				finder.addPath(Gdx.files.internal(Environment.BASE_DIR + Config.SOUNDS_DIRECTORY));
				finder.setFilterSuffix(".mp3");
				for (FileHandle file : finder.find()) {
					if (Config.DEVEL)
						Gdx.app.log(ConfigCore.TAG, "Queue | Sounds | " + file.path());
					assetManager.load(file.path(), Sound.class);
				}

				// Global Music
			}
		});
		// Set initialized.
		initialized = true;
	}

	/**
	 * Add an interface to {@link Core}.
	 * 
	 * @param name Name of the interface. Set by implementor.
	 * @param interf The interface implementation.
	 */
	public void addInterface(String name, IBase interf) {
		mapInterfaces.put(name, interf);
	}

	/**
	 * Add an array of interface to {@link Core}.
	 * 
	 * @param interf The interface implementation.
	 */
	public void addInterfaces(IBase[] interfs) {
		for (IBase i : interfs) {
			addInterface(i.getName(), i);
		}
	}

	/**
	 * Return an interface by name.
	 * 
	 * @param name Name of interface.
	 * @return {@link IBase} interface.
	 */
	public IBase getInterface(String name) {
		return mapInterfaces.get(name);
	}

	/**
	 * Return the collection of interfaces.
	 * 
	 * @return {@link ObjectMap} object.
	 */
	public ObjectMap<String, IBase> getInterfaces() {
		return mapInterfaces;
	}

	/**
	 * Return the global asset manager.
	 * 
	 * @return {@link AssetManager}
	 */
	public AssetManager getAssetManager() {
		return assetManager;
	}

	/**
	 * Dispose all disposable.
	 */
	public void dispose() {
		if (assetManager != null)
			assetManager.dispose();
		getTextureManager().dispose();
	}

	/**
	 * Return the default settings storage.
	 * 
	 * @return {@link Settings} object.
	 */
	public Settings getSettings() {
		return getSettings(ConfigCore.SETTINGS_DEFAULT_NAME);
	}

	/**
	 * Return the settings storage by name.
	 * 
	 * @param name Associated name for settings.
	 * @return {@link Settings} object.
	 */
	public Settings getSettings(String name) {
		if (!mapSettings.containsKey(name)) {
			Settings settings = new Settings(Config.PACKAGE + "." + name);
			mapSettings.put(name, settings);
			return settings;
		} else {
			return mapSettings.get(name);
		}
	}

	/**
	 * Clears all settings.
	 */
	public void clearSettings() {
		for (Entry<String, Settings> entry : mapSettings) {
			entry.value.clear();
		}
	}

	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * Return the http client object.
	 * 
	 * @return {@link HttpClient} object.
	 */
	public HttpClient getHttpClient() {
		return httpClient == null ? httpClient = new HttpClient() : httpClient;
	}

	/**
	 * Return the sound manager.
	 * 
	 * @return {@link SoundManagerOld} object.
	 */
	public SoundManager getSoundManager() {
		return soundManager;
	}

	/**
	 * Return the screen manager.
	 * 
	 * @return {@link ScreenManager} object.
	 */
	public ScreenManager getScreenManager() {
		return ScreenManager.getInstance();
	}

	/**
	 * Return the texture manager.
	 * 
	 * @return {@link TextureManager} object.
	 */
	public TextureManager getTextureManager() {
		return TextureManager.getInstance();
	}

}
