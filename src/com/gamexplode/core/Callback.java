package com.gamexplode.core;

public interface Callback<T> {
	public void apply(T t);
}
