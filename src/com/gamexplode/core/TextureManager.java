package com.gamexplode.core;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * TextureManager is a singleton that manages the use of textures.
 * 
 * This class depends on Core's AssetManager member. Before you use atlas, make sure it is loaded in asset manager.
 * 
 * @author Kevin
 */
public class TextureManager {

	private static TextureManager INSTANCE;
	private static final String DELIMITER = "?";
	private static final int DEFAULT_INDEX = -1;
	private static final String DEFAULT_EXT = "png";

	private TextureAtlas currentTextureAtlas;
	private String currentTextureAtlasName;
	private String currentTextureDirectory;
	private String baseDir;
	private String fileExt;

	private ObjectMap<String, AtlasRegion> cachedAtlasRegions;
	private ObjectMap<String, Texture> cachedTextures;

	private TextureFilter filterMin;
	private TextureFilter filterMag;
	private AssetManager assetManager;

	public TextureManager() {
		if (!Core.i().isInitialized()) {
			throw new GdxRuntimeException("Core must be initialized before instantiating texture manager.");
		}
		baseDir = Environment.BASE_DIR;
		fileExt = ".atlas";
		cachedAtlasRegions = new ObjectMap<String, TextureAtlas.AtlasRegion>();
		cachedTextures = new ObjectMap<String, Texture>();
		filterMin = TextureFilter.Linear;
		filterMag = TextureFilter.Linear;
		assetManager = AssetManager.i();
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * 
	 * @return {@link TextureManager} object.
	 */
	public static TextureManager getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * 
	 * @return {@link TextureManager} object.
	 */
	public static TextureManager i() {
		if (INSTANCE == null) {
			INSTANCE = new TextureManager();
		}
		return INSTANCE;
	}

	/**
	 * Sets the filter for all textures.
	 * 
	 * @param minFilter
	 * @param magFilter
	 */
	public void setFilter(TextureFilter minFilter, TextureFilter magFilter) {
		filterMin = minFilter;
		filterMag = magFilter;
	}

	/**
	 * Get the specified atlas.
	 * 
	 * @param atlas
	 * @return
	 */
	public TextureAtlas getAtlas(String atlas) {
		return assetManager.get(baseDir + atlas + fileExt, TextureAtlas.class);
	}

	/**
	 * Sets the current atlas to work with.
	 * 
	 * @param atlas String atlas name.
	 */
	public void useAtlas(String atlas) {
		currentTextureAtlas = getAtlas(atlas);
		if (currentTextureAtlas != null) {
			currentTextureAtlasName = atlas;
		}
	}

	public void useDirectory(String directory) {
		currentTextureDirectory = baseDir + directory;
	}

	/**
	 * Get a region (texture) from the current atlas and cache it for future use. Does not use sibling member getRegion(String, String) because it
	 * will exert extra cost for finding the current atlas.
	 * 
	 * @param name String name if region.
	 * @return {@link AtlasRegion} object.
	 */
	public AtlasRegion getRegion(String name) {
		return getRegion(name, DEFAULT_INDEX);
	}

	public AtlasRegion getRegion(String name, int index) {
		if (currentTextureAtlas == null) {
			throw new GdxRuntimeException("No current texture atlas. Can be set by useAtlas()");
		}
		AtlasRegion region = cachedAtlasRegions.get(currentTextureAtlasName + DELIMITER + name + index);
		if (region == null) {
			if (index == DEFAULT_INDEX) {
				region = currentTextureAtlas.findRegion(name);
			} else {
				region = currentTextureAtlas.findRegion(name, index);
			}
			if (region != null) {
				region.getTexture().setFilter(filterMin, filterMag);
				cachedAtlasRegions.put(currentTextureAtlasName + DELIMITER + name + index, region);
			} else {
				throw new GdxRuntimeException("No region " + name + index + " found in current texture atlas " + currentTextureAtlasName + ".");
			}
		}
		return region;
	}

	/**
	 * Get a region (texture) from specified atlas by name and cache it for future use.
	 * 
	 * @param atlas
	 * @param name
	 * @return
	 */
	public AtlasRegion getRegion(String atlas, String name) {
		return getRegion(atlas, name, DEFAULT_INDEX);
	}

	public AtlasRegion getRegion(String atlas, String name, int index) {
		AtlasRegion region = cachedAtlasRegions.get(atlas + DELIMITER + name + index);
		if (region == null) {
			if (index == DEFAULT_INDEX) {
				region = getAtlas(atlas).findRegion(name);
			} else {
				region = getAtlas(atlas).findRegion(name, index);
			}
			if (region != null) {
				region.getTexture().setFilter(filterMin, filterMag);
				cachedAtlasRegions.put(atlas + DELIMITER + name + index, region);
			} else {
				throw new GdxRuntimeException("No region " + name + index + " found in " + atlas + " texture atlas.");
			}
		}
		return region;
	}

	/**
	 * Closing the currently use atlas.
	 */
	public void close() {
		currentTextureAtlas = null;
		currentTextureAtlasName = null;
	}

	/**
	 * Load and get texture by image file. Uses default file extension ".png". This method cached the texture.
	 * 
	 * @param path String file path.
	 * @return {@link TextureRegion} object.
	 */
	public Texture getTextureFromFile(String path) {
		return getTextureFromFile(path, DEFAULT_EXT);
	}

	/**
	 * Load and get texture by image file. This method cached the texture.
	 * 
	 * @param path String file path.
	 * @param ext String extension name of file.
	 * @return {@link Texture} object.
	 */
	public Texture getTextureFromFile(String path, String ext) {
		path = path + "." + ext;
		Texture tr = cachedTextures.get(path);
		if (tr == null) {
			tr = new Texture(path);
			tr.setFilter(filterMin, filterMag);
			cachedTextures.put(path, tr);
		}
		return tr;
	}

	/**
	 * Extract a region from its atlas to {@link Pixmap}.
	 * 
	 * WARNING! This does NOT use cache and might lead to overhead. Therefore, avoid frequent call and always dispose {@link Pixmap}.
	 * 
	 * @param region.
	 */
	public Pixmap extractToPixmap(TextureRegion region) {
		TextureData textureData = region.getTexture().getTextureData();
		if (!textureData.isPrepared()) {
			textureData.prepare();
		}
		int w = region.getRegionWidth(), h = region.getRegionHeight();
		Pixmap atlasPixmap = textureData.consumePixmap();
		Pixmap regionPixmap = new Pixmap(w, h, Format.RGBA8888);
		regionPixmap.drawPixmap(atlasPixmap, region.getRegionX(), region.getRegionY(), w, h, 0, 0, w, h);
		atlasPixmap.dispose();
		// textureData.disposePixmap();
		return regionPixmap;
	}

	/**
	 * Load a region from current atlas and return it as a {@link Pixmap}.
	 * 
	 * WARNING! This does NOT use cache and might lead to overhead. Therefore, always dispose {@link Pixmap}.
	 * 
	 * @param path String file path.
	 */
	public Pixmap getPixmap(String name) {
		return extractToPixmap(getRegion(name));
	}

	/**
	 * Extract a region from specified atlas and return it as a {@link Pixmap}.
	 * 
	 * WARNING! This does NOT use cache and might lead to overhead. Therefore, always dispose {@link Pixmap}.
	 * 
	 * @param path String file path.
	 */
	public Pixmap getPixmap(String atlas, String name) {
		return extractToPixmap(getAtlas(atlas).findRegion(name));
	}

	public Pixmap getPixmap(String name, int index) {
		return extractToPixmap(getRegion(name, index));
	}

	public Pixmap getPixmap(String atlas, String name, int index) {
		return extractToPixmap(getAtlas(atlas).findRegion(name, index));
	}

	/**
	 * Get a region from current atlas and extract it as {@link Texture}. This use cache.
	 * 
	 * @param path String file path.
	 */
	public Texture getRegionTexture(String name) {
		return getRegionTexture(name, DEFAULT_INDEX);
	}

	public Texture getRegionTexture(String name, int index) {
		if (currentTextureAtlas == null) {
			throw new GdxRuntimeException("No current texture atlas.");
		}
		Texture texture = cachedTextures.get(currentTextureAtlasName + DELIMITER + name);
		if (texture == null) {
			Pixmap pm = getPixmap(name, index);
			texture = new Texture(pm);
			pm.dispose();
			texture.setFilter(filterMin, filterMag);
			cachedTextures.put(currentTextureAtlasName + DELIMITER + name, texture);
		}
		return texture;
	}

	/**
	 * Get a region from specified atlas and extract it as {@link Texture}. This use cache.
	 * 
	 * @param path String file path.
	 */
	public Texture getRegionTexture(String atlas, String name) {
		return getRegionTexture(atlas, name, DEFAULT_INDEX);
	}

	public Texture getRegionTexture(String atlas, String name, int index) {
		if (currentTextureAtlas == null) {
			throw new GdxRuntimeException("No current texture atlas.");
		}
		Texture texture = cachedTextures.get(atlas + DELIMITER + name);
		if (texture == null) {
			Pixmap pm = getPixmap(atlas, name, index);
			texture = new Texture(pm);
			pm.dispose();
			texture.setFilter(filterMin, filterMag);
			cachedTextures.put(atlas + DELIMITER + name, texture);
		}
		return texture;
	}

	/**
	 * Get a texture from specified directory, name and extension. This use cache.
	 * 
	 * @param directory
	 * @param name
	 * @return
	 */
	public Texture getTexture(String directory, String name, String ext) {
		if (!directory.endsWith("/"))
			directory += "/";
		name = directory + name + "." + ext;
		Texture tr = cachedTextures.get(name);
		if (tr == null) {
			tr = assetManager.get(name, Texture.class);
			tr.setFilter(filterMin, filterMag);
			cachedTextures.put(name, tr);
		}
		return tr;
	}

	public Texture getTexture(String directory, String name) {
		return getTexture(directory, name, DEFAULT_EXT);
	}

	/**
	 * Get a texture from current used directory with default extension format. This use cache.
	 * 
	 * @param name
	 * @return
	 */
	public Texture getTexture(String name) {
		if (currentTextureDirectory == null) {
			throw new GdxRuntimeException("No current texture directory. Can be set by useDirectory()");
		}
		return getTexture(currentTextureDirectory, name, DEFAULT_EXT);
	}

	/**
	 * Set the wrap style for a texture.
	 * 
	 * @param directory
	 * @param name
	 * @param ext
	 * @param u
	 * @param v
	 */
	public void setTextureWrap(String directory, String name, String ext, TextureWrap u, TextureWrap v) {
		getTexture(directory, name, ext).setWrap(u, v);
	}

	public void setTextureWrap(String directory, String name, TextureWrap u, TextureWrap v) {
		getTexture(directory, name).setWrap(u, v);
	}

	public void setTextureWrap(String name, TextureWrap u, TextureWrap v) {
		getTexture(name).setWrap(u, v);
	}

	/**
	 * Dispose objects.
	 */
	public void dispose() {
		// No need to dispose TextureAtlas here because AssetManager handles it implicitly.
		// currentTextureAtlas.dispose();
		cachedAtlasRegions.clear();
		cachedTextures.clear();
	}

}
