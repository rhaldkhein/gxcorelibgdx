package com.gamexplode.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class Config {

	private static Properties _PROPERTIES;

	public static final String NAME = getString("name");
	public static final String PACKAGE = getString("package");
	public static final String VERSION = getString("version");
	public static final String DESCRIPTION = getString("description");

	public static final int LOG_LEVEL = getInt("log_level");
	public static final boolean DEVEL = getBoolean("development");
	public static final boolean PROD = getBoolean("production");

	public static final String STORE_URL_IOS = getString("store_url_ios");
	public static final String STORE_URL_ANDROID = getString("store_url_android");
	public static final String STORE_URL_OTHER = getString("store_url_other");

	public static final boolean LOADSCREEN_ENABLED = getBoolean("loadscreen_enabled");
	public static final String LOADSCREEN_ATLAS = getString("loadscreen_atlas");

	public static final String TEXTURES_DIRECTORY = getString("textures_directory");
	public static final String ATLAS_DIRECTORY = getString("atlas_directory");
	public static final String FONTS_DIRECTORY = getString("fonts_directory");
	public static final String SOUNDS_DIRECTORY = getString("sounds_directory");
	public static final String PARTICLES_DIRECTORY = getString("particles_directory");

	public static final float WIDTH = getFloat("width");
	public static final float HEIGHT = getFloat("height");

	public static final float WORLD_WIDTH = getFloat("world_width");
	public static final float WORLD_HEIGHT = getFloat("world_height");

	public static final int PPU = getPixelPerUnit();
	public static final float PPUF = (float) PPU;

	/**
	 * Class constructor. Protected to prevent object creation.
	 */
	protected Config() {
		// Prevent instantiation
	}

	/**
	 * Initialize global configuration.
	 * 
	 * @param file Complete path to properties file
	 */
	private static void readConfig(FileHandle file) {
		Properties defaultProperties = new Properties();

		// -------------------------------------------------------
		// Optional configuration properties

		defaultProperties.setProperty("description", "");

		defaultProperties.setProperty("log_level", "3");
		defaultProperties.setProperty("development", "true");
		defaultProperties.setProperty("production", "false");

		defaultProperties.setProperty("store_url_ios", "https://itunes.apple.com");
		defaultProperties.setProperty("store_url_android", "https://play.google.com");
		defaultProperties.setProperty("store_url_other", "");

		defaultProperties.setProperty("loadscreen_enabled", "false");
		defaultProperties.setProperty("loadscreen_atlas", "loadscreen/loadscreen.atlas");

		defaultProperties.setProperty("textures_directory", "textures");
		defaultProperties.setProperty("atlas_directory", "atlas");
		defaultProperties.setProperty("fonts_directory", "fonts");
		defaultProperties.setProperty("sounds_directory", "sounds");
		defaultProperties.setProperty("particles_directory", "particles");

		defaultProperties.setProperty("width", "720"); // pixel, canvas draw size
		defaultProperties.setProperty("height", "1280"); // pixel, canvas draw size

		defaultProperties.setProperty("world_width", "9"); // unit, world size, ppu
		defaultProperties.setProperty("world_height", "16"); // unit, world size, ppu

		// Optional configuration properties
		// -------------------------------------------------------

		_PROPERTIES = new Properties(defaultProperties);
		try {
			_PROPERTIES.load(file.read());
		} catch (FileNotFoundException e) {
			// TODO Ask to re-install the application.
			Gdx.app.error(ConfigCore.TAG, e.getMessage());
		} catch (IOException e) {
			// TODO Show error and contact support dialog and terminate.
			Gdx.app.error(ConfigCore.TAG, e.getMessage());
		}
	}

	/**
	 * Get a string property.
	 * 
	 * @param key String key
	 * @return String value
	 */
	public static String getString(String key) {
		try {
			if (_PROPERTIES == null) {
				readConfig(Gdx.files.classpath("config.properties"));
			}
			String value = _PROPERTIES.getProperty(key);
			if (value == null) {
				throw new GdxRuntimeException("Config property key \"" + key + "\" is not found.");
			}
		} catch (Exception e) {
			// TODO Show error and contact support dialog and terminate.
			Gdx.app.error(ConfigCore.TAG, e.getMessage());
		}
		return _PROPERTIES.getProperty(key);
	}

	/**
	 * Get a string property and parse to float.
	 * 
	 * @param key String key
	 * @return Float value
	 */
	public static float getFloat(String key) {
		return Float.parseFloat(getString(key));
	}

	/**
	 * Get a string property and parse to integer.
	 * 
	 * @param key String key
	 * @return Integer value
	 */
	public static int getInt(String key) {
		return Integer.parseInt(getString(key));
	}

	/**
	 * Get a string property and parse to boolean.
	 * 
	 * @param key String key
	 * @return Boolean value
	 */
	public static boolean getBoolean(String key) {
		return Boolean.parseBoolean(getString(key));
	}

	/**
	 * Get array of string.
	 * 
	 * @param key String key
	 * @return String[]
	 */
	public static String[] getStringArray(String key) {
		return getString(key).split(",");
	}

	/**
	 * Return the pixels per unit based on world size and game's canvas size. PPU = HEIGHT / WORLD_HEIGHT
	 * 
	 * @return {@link Integer}
	 */
	public static int getPixelPerUnit() {
		return (int) (HEIGHT / WORLD_HEIGHT);
	}

}
