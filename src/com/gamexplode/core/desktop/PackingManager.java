package com.gamexplode.core.desktop;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public final class PackingManager {

	private boolean isModified;
	private boolean isForced;
	private PackingList mList;

	public PackingManager() {
		isModified = false;
		isForced = false;
	}

	public void pack(PackingList list) {
		isModified = false;
		mList = list;
		Settings settings = list.getSettings();
		String input, output, filename;
		try {
			for (String[] packDir : list.getList()) {
				input = packDir[0];
				output = packDir[1];
				filename = packDir[2];
				File inputDir = new File(input);
				File outputFile = new File(output);
				if (!outputFile.exists()) {
					Files.createDirectories(outputFile.toPath());
				}
				if (inputDir.isDirectory() && inputDir.listFiles().length > 0) {
					if (list.forcePack) {
						TexturePacker.process(settings, input, output, filename);
						isForced = true;
					} else if (TexturePacker.isModified(input, output, filename, settings)) {
						TexturePacker.processIfModified(settings, input, output, filename);
						isModified = true;
					}
				}
			}
			for (String[] copyDir : list.getCopyList()) {
				input = copyDir[0];
				output = copyDir[1];
				File inputFile = new File(input);
				File outputFile = new File(output);
				if (!outputFile.exists()) {
					Files.createDirectories(outputFile.toPath());
				}
				if (inputFile.isDirectory() && copy(input, output)) {
					isModified = true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean modified() {
		return isModified;
	}

	public boolean forced() {
		return isForced;
	}

	public void deleteFolder(File folder) {
		File[] files = folder.listFiles();
		if (files != null) { // some JVMs return null for empty dirs
			for (File f : files) {
				if (f.isDirectory()) {
					deleteFolder(f);
				} else {
					f.delete();
				}
			}
		}
		folder.delete();
	}

	public boolean isModified(String input, String output) {
		File inputFile = new File(input);
		File outputFile = new File(output);
		if (!outputFile.exists()) {
			return true;
		}
		if (!inputFile.exists()) {
			throw new IllegalArgumentException("Input file does not exist: " + inputFile.getAbsolutePath());
		}
		return inputFile.lastModified() > outputFile.lastModified();
	}

	public boolean copy(String input, String output) throws IOException {
		File inputFile = new File(input);
		boolean modified = false;
		for (File file : inputFile.listFiles()) {
			File outputFile = new File(output + File.separator + file.getName());
			if (file.isFile()) {
				if (mList.forcePack || isModified(file.getPath(), outputFile.getPath())) {
					Files.copy(file.toPath(), outputFile.toPath(), REPLACE_EXISTING);
					modified = true;
				}
			} else if (file.isDirectory()) {
				if (!outputFile.exists()) {
					Files.createDirectories(outputFile.toPath());
				}
				modified = copy(file.getPath(), outputFile.getPath());
			}
		}
		return modified;
	}

}
