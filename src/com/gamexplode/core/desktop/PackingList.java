package com.gamexplode.core.desktop;

import java.io.File;
import java.util.ArrayList;

import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public abstract class PackingList {

	private ArrayList<String[]> mList;
	private ArrayList<String[]> mCopyList;
	private Settings mSettings;
	private String mOutputDirDefault;
	public boolean forcePack;

	public PackingList() {
		mList = new ArrayList<String[]>();
		mCopyList = new ArrayList<String[]>();
		generateSettings();
		generateList();
	}

	public abstract Settings generateSettings();

	public abstract void generateList();

	public void setDefaultOutputDir(String output) {
		mOutputDirDefault = output;
	}

	public void setSettings(Settings settings) {
		mSettings = settings;
	}

	public Settings getSettings() {
		return mSettings != null ? mSettings : new Settings();
	}

	public ArrayList<String[]> getList() {
		return mList;
	}

	public ArrayList<String[]> getCopyList() {
		return mCopyList;
	}

	protected void addDirectory(String dirInput) {
		addDirectory(dirInput, mOutputDirDefault);
	}

	protected void addDirectory(String dirInput, String dirOutput) {
		File file = new File(dirInput);
		mList.add(new String[] { dirInput, dirOutput, file.getName() });
	}

	protected void addDirectories(String parentDirInput) {
		addDirectories(parentDirInput, mOutputDirDefault);
	}

	protected void addDirectories(String parentDirInput, String dirOuput) {
		File fileParent = new File(parentDirInput);
		for (File file : fileParent.listFiles()) {
			if (file.isDirectory()) {
				addDirectory(file.getAbsolutePath(), dirOuput);
			}
		}
	}
	
	protected void addCopyDirectory(String dirInput) {
		addCopyDirectory(dirInput, mOutputDirDefault);
	}

	protected void addCopyDirectory(String dirInput, String dirOutput) {
		File file = new File(dirInput);
		mCopyList.add(new String[] { dirInput, dirOutput, file.getName() });
	}

	protected void addCopyDirectories(String parentDirInput, String dirOuput) {
		File fileParent = new File(parentDirInput);
		for (File file : fileParent.listFiles()) {
			if (file.isDirectory()) {
				addCopyDirectory(file.getAbsolutePath(), dirOuput);
			}
		}
	}

}
