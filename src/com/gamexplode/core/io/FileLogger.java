package com.gamexplode.core.io;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class FileLogger {

	private FileHandle fileLog;

	public FileLogger(String filename) {
		if (Gdx.files.isExternalStorageAvailable()) {
			fileLog = Gdx.files.external(filename);
			fileLog.writeString("", false);
		}
	}

	public void write(String tag, String msg) {
		if (fileLog != null) {
			fileLog.writeString(tag + ": " + msg + " ", true);
		}
	}

}
