package com.gamexplode.core.io;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import com.badlogic.gdx.files.FileHandle;

public class FileFinder {

	private static final String REGEX_FILE = ".+\\.[^\\/\\\\]+$";

	private ArrayList<FileHandle> _foundFiles;
	private ArrayList<FileHandle> _findPaths;
	private FileFilter _filter;
	private boolean _recursive;
	private boolean _includeDir;

	public FileFinder() {
		_recursive = false;
		_includeDir = false;
	}

	public void setRecursive(boolean recursive) {
		_recursive = recursive;
	}

	public void setIncludeDir(boolean includeDir) {
		_includeDir = includeDir;
	}

	public void addPath(FileHandle path) {
		if (_findPaths == null) {
			_findPaths = new ArrayList<FileHandle>();
		}
		if (!_findPaths.contains(path)) {
			_findPaths.add(path);
		}
	}

	public void setFilterSuffix(final String suffix) {
		setFilter(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				String path = pathname.getPath();
				//Gdx.app.log(ConfigCore.TAG, "Match | " + path + " | " + path.matches(REGEX_FILE));
				if (!path.matches(REGEX_FILE)) {
					// Directory
					return true;
				} else if (path.endsWith(suffix)) {
					return true;
				}
				return false;
			}
		});
	}

	public void setFilter(final String filterRegex) {
		setFilter(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				String path = pathname.getPath();
				if (!path.matches(REGEX_FILE)) {
					// Directory
					return true;
				} else if (path.matches(filterRegex)) {
					return true;
				}
				return false;
			}
		});
	}

	public void setFilter(FileFilter filter) {
		_filter = filter;
	}

	public FileHandle[] find() {
		if (_foundFiles == null) {
			_foundFiles = new ArrayList<FileHandle>();
		}
		for (FileHandle path : _findPaths) {
			_doFind(path);
		}
		return _foundFiles.toArray(new FileHandle[_foundFiles.size()]);
	}

	private void _doFind(FileHandle path) {

		if (_filter == null) {
			// Gdx.app.log(ConfigCore.TAG, "FileFinder | _doFind | no filter | getting all files");
			setFilter(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return true;
				}
			});
		}

		for (FileHandle file : path.list(_filter)) {
			// Gdx.app.log(ConfigCore.TAG, "FileFinder | _doFind | path: " + path.path() + " | found: " + file.path());
			if (file.isDirectory()) {
				if (_includeDir) {
					_foundFiles.add(file);
				}
				if (_recursive) {
					_doFind(file);
				}
			} else {
				_foundFiles.add(file);
			}
		}

	}

	public void reset() {
		_foundFiles.clear();
		_filter = null;
	}

	public void reset(boolean clearPath) {
		reset();
		if (clearPath) {
			clearPaths();
		}
	}

	public void clearPaths() {
		_findPaths.clear();
	}

}
