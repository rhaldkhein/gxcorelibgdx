package com.gamexplode.core;

import java.io.InputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.ObjectMap;

public class HttpClient {

	private String defaultClient = "default";
	private ObjectMap<String, String> mapCookies;
	private ObjectMap<String, TextureRegion> mapTextures;
	private boolean isOpen;

	public void open() {
		mapCookies = new ObjectMap<String, String>();
		mapTextures = new ObjectMap<String, TextureRegion>();
		isOpen = true;
	}

	public void create(String domain) {
		if (domain.isEmpty()) {
			domain = defaultClient;
		}
		mapCookies.put(domain, "");
	}

	public void image(final String url, final Callback<TextureRegion> callback) {
		if (mapTextures.containsKey(url)) {
			callback.apply(mapTextures.get(url));
		} else {
			get(url, new HttpResponseListener() {
				public void handleHttpResponse(HttpResponse httpResponse) {
					byte[] bytes = httpResponse.getResult();
					Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
					final int originalWidth = pixmap.getWidth();
					final int originalHeight = pixmap.getHeight();
					int width = MathUtils.nextPowerOfTwo(pixmap.getWidth());
					int height = MathUtils.nextPowerOfTwo(pixmap.getHeight());
					final Pixmap potPixmap = new Pixmap(width, height, pixmap.getFormat());
					potPixmap.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.getWidth(), pixmap.getHeight());
					pixmap.dispose();
					Gdx.app.postRunnable(new Runnable() {
						@Override
						public void run() {
							Texture t = new Texture(potPixmap);
							t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
							TextureRegion tr = new TextureRegion(t, 0, 0, originalWidth, originalHeight);
							callback.apply(tr);
							mapTextures.put(url, tr);
						}
					});
				}

				public void failed(Throwable t) {
					callback.apply(null);
				}

				public void cancelled() {
					callback.apply(null);
				}
			});
		}

	}

	public void get(String url, HttpResponseListener listener) {
		get(url, null, listener);
	}

	public void get(String url, Map<String, String> parameters, HttpResponseListener listener) {
		HttpRequest httpReq = new HttpRequest(HttpMethods.GET);
		httpReq.setUrl(url);
		if (parameters != null && !parameters.isEmpty()) {
			// Gdx.app.debug("HttpClient", "GET | " + url + " | " + HttpParametersUtils.convertHttpParameters(parameters));
			httpReq.setContent(HttpParametersUtils.convertHttpParameters(parameters));
		}
		request(httpReq, listener);
	}

	public void post(String url, Map<String, String> parameters, HttpResponseListener listener) {
		HttpRequest httpReq = new HttpRequest(HttpMethods.POST);
		httpReq.setUrl(url);
		httpReq.setContent(HttpParametersUtils.convertHttpParameters(parameters));
		request(httpReq, listener);
	}

	public void post(String url, InputStream contentStream, long contentLength, HttpResponseListener listener) {
		HttpRequest httpReq = new HttpRequest(HttpMethods.POST);
		httpReq.setUrl(url);
		httpReq.setContent(contentStream, contentLength);
		request(httpReq, listener);
	}

	public void delete(String url, HttpResponseListener listener) {
		delete(url, null, listener);
	}

	public void delete(String url, Map<String, String> parameters, HttpResponseListener listener) {
		HttpRequest httpReq = new HttpRequest(HttpMethods.DELETE);
		httpReq.setUrl(url);
		if (parameters != null && !parameters.isEmpty()) {
			httpReq.setContent(HttpParametersUtils.convertHttpParameters(parameters));
		}
		request(httpReq, listener);
	}

	public void put(String url, HttpResponseListener listener) {
		put(url, null, listener);
	}

	public void put(String url, Map<String, String> parameters, HttpResponseListener listener) {
		HttpRequest httpReq = new HttpRequest(HttpMethods.PUT);
		httpReq.setUrl(url);
		if (parameters != null && !parameters.isEmpty()) {
			httpReq.setContent(HttpParametersUtils.convertHttpParameters(parameters));
		}
		request(httpReq, listener);
	}

	public void request(HttpRequest request, final HttpResponseListener listener) {
		final String domainName = getDomainName(request.getUrl());
		String cookie = mapCookies.get(domainName);
		if (cookie == null) {
			create(domainName);
			cookie = mapCookies.get(domainName);
		}
		if (!cookie.isEmpty()) {
			request.setHeader("Cookie", "PHPSESSID=" + cookie);
		}
		request.setFollowRedirects(true);
		Gdx.net.sendHttpRequest(request, new HttpResponseListener() {
			public void handleHttpResponse(HttpResponse httpResponse) {
				String cookieString = httpResponse.getHeader("Set-Cookie");
				if (cookieString != null) {
					String[] keyValPairs = cookieString.split("; ?");
					for (String encodedPair : keyValPairs) {
						String keyVal[] = encodedPair.split("=");
						if (keyVal[0].equalsIgnoreCase("PHPSESSID")) {
							mapCookies.put(domainName, keyVal[1].trim());
						}
					}
				}
				listener.handleHttpResponse(httpResponse);
			}

			@Override
			public void failed(Throwable t) {
				listener.failed(t);
			}

			@Override
			public void cancelled() {
				listener.cancelled();
			}
		});
	}

	private String getDomainName(String url) {
		Pattern pattern = Pattern.compile("//(.*?)/");
		Matcher matcher = pattern.matcher(url);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return "";
	}

	public boolean hasCookie(String domain) {
		return mapCookies.containsKey(domain);
	}
	
	public String getCookie(String domain) {
		return hasCookie(domain) ? mapCookies.get(domain) : "";
	}
	
	public boolean isOpen() {
		return isOpen;
	}

}
