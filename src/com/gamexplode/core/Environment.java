package com.gamexplode.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;

public class Environment {

	public final static String BASE_DIR = getBaseDirectory();
	
	private static String getBaseDirectory() {
		String base = "";
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			base = "./bin/";
		}
		return base;
	}
	
}
