package com.gamexplode.core;


public class ConfigCore {
	
	public final static String TAG = "GxCoreLibGdx";
	
	public final static String NAME = "Gx Core";
	public final static String VERSION = "1.0.0";
	
	public final static String KEY_FILE = "randomkey";
	
	public final static String SETTINGS_DEFAULT_NAME = "default";
	
}
