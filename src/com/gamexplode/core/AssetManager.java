package com.gamexplode.core;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.gamexplode.core.screen.ScreenManager;

public class AssetManager extends com.badlogic.gdx.assets.AssetManager {

	public static FileHandleResolver RESOLVER;
	private static AssetManager INSTANCE;

	private AssetFontGenerator _fontGenerator;
	private boolean _loading;
	private Callback<Object> _preloadCallback;
	private Callback<Object> _postloadCallback;

	public AssetManager() {
		super();
		_loading = false;
	}

	public AssetManager(FileHandleResolver resolver) {
		super(resolver);
		_loading = false;
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * 
	 * @return {@link ScreenManager} object.
	 */
	public static AssetManager getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * 
	 * @return {@link ScreenManager} object.
	 */
	public static AssetManager i() {
		if (INSTANCE == null) {
			INSTANCE = RESOLVER == null ? new AssetManager() : new AssetManager(RESOLVER);
		}
		return INSTANCE;
	}

	@Override
	public synchronized boolean update() {
		// Check if we have pre or post load callback, and call it.
		return _update(super.update());
	}

	@Override
	public boolean update(int millis) {
		// Check if we have pre or post load callback, and call it.
		return _update(super.update(millis));
	}

	public boolean _update(boolean parentUpdate) {
		if (parentUpdate) {
			// done loading, call the post load callback.
			if (_postloadCallback != null) {
				_postloadCallback.apply(null);
				_postloadCallback = null;
			}
			_loading = false;
		} else {
			// still loading
			if (!_loading) {
				// first update, call the pre load callback
				if (_preloadCallback != null) {
					_preloadCallback.apply(null);
					_preloadCallback = null;
				}
				_loading = true;
			}
		}
		return parentUpdate;
	}

	public void loadSetup(AssetLoadSetup asset) {
		asset.setup();
	}

	public void setPreloadCallback(Callback<Object> preloadCallback) {
		_preloadCallback = preloadCallback;
	}

	public void setPostloadCallback(Callback<Object> postloadCallback) {
		_postloadCallback = postloadCallback;
	}

	public void setFontGenerator(AssetFontGenerator generator) {
		_fontGenerator = generator;
	}

	public AssetFontGenerator getFontGenerator() {
		return _fontGenerator;
	}

}
