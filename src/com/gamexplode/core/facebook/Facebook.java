package com.gamexplode.core.facebook;

import com.gamexplode.core.Callback;
import com.gamexplode.core.Core;

public class Facebook {

	private static IFacebook facebook;
	private static Callback<String> callback;

	public static void open(Callback<String> statusCallback) {
		callback = statusCallback;
		if (facebook == null) {
			facebook = (IFacebook) Core.i().getInterface("facebook");
		}
		if (facebook != null) {
			facebook.setStatusChangeCallback(new Callback<String>() {
				@Override
				public void apply(String status) {
					if (status == "OPENED") {
						fetchFriends();
					}
					if (callback != null)
						callback.apply(status);
				}
			});
			facebook.open();
		}
	}

	public static void fetchFriends() {
		
	}

}
