package com.gamexplode.core.facebook;

import com.gamexplode.core.Callback;
import com.gamexplode.core.IBase;

public interface IFacebook extends IBase {
	
	public abstract void open();

	public abstract void setStatusChangeCallback(Callback<String> callback);
	
	public abstract String getAccessToken();

}
