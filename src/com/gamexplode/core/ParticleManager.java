package com.gamexplode.core;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;

public class ParticleManager {

	private static ParticleManager INSTANCE;
	private String baseDir;
	private String fileExt;
	private String currentDir;
	private AssetManager assetManager;

	public ParticleManager() {
		baseDir = Environment.BASE_DIR;
		fileExt = ".particle";
		assetManager = AssetManager.i();
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * @return {@link TextureManager} object.
	 */
	public static ParticleManager getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * @return {@link TextureManager} object.
	 */
	public static ParticleManager i() {
		if (INSTANCE == null) {
			INSTANCE = new ParticleManager();
		}
		return INSTANCE;
	}

	/**
	 * Add slash to end of string
	 * @param pathDir
	 * @return
	 */
	private String fixDirectory(String pathDir) {
		return pathDir.endsWith("/") ? pathDir : pathDir + "/";
	}

	/**
	 * Set & use specified directory.
	 * @param pathDir
	 */
	public void useDirectory(String pathDir) {
		currentDir = fixDirectory(pathDir);
	}

	/**
	 * Get a single particle effect.
	 * @param name
	 * @return
	 */
	public ParticleEffect getParticle(String name) {
		String dir = currentDir;
		if (dir == null) {
			dir = fixDirectory(Config.PARTICLES_DIRECTORY);
		}
		return assetManager.get(baseDir + dir + name + fileExt, ParticleEffect.class);
	}

}
