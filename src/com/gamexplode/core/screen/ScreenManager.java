package com.gamexplode.core.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.gamexplode.core.AssetManager;
import com.gamexplode.core.Config;
import com.gamexplode.core.Core;
import com.gamexplode.core.IBase;

/**
 * ScreenManager is a singleton that manages the switching between screens.
 * 
 * @author Kevin
 */
public class ScreenManager extends Game {

	private static ScreenManager INSTANCE;
	private Color backgroundColor;
	private InputMultiplexer input;
	private Class<? extends LoadScreen> loadScreenType;
	
	/**
	 * Instance constructor.
	 * 
	 * @param interfaces 3rd party libraries.
	 */
	public ScreenManager(IBase... interfaces) {
		Core.i().addInterfaces(interfaces);
	}

	/**
	 * Long hand to get singleton instance by calling the short hand.
	 * 
	 * @return {@link ScreenManager} object.
	 */
	public static ScreenManager getInstance() {
		return i();
	}

	/**
	 * Short hand to get singleton instance of this class.
	 * 
	 * @return {@link ScreenManager} object.
	 */
	public static ScreenManager i() {
		return INSTANCE;
	}

	@Override
	public void create() {
		// Reference single instance.
		INSTANCE = this;
		// Set log level.
		Gdx.app.setLogLevel(Config.LOG_LEVEL);
		// Enable multiple input processor.
		input = new InputMultiplexer();
		Gdx.input.setInputProcessor(input);
		// Set default background color.
		backgroundColor = new Color(0.2f, 0.2f, 0.2f, 1);
	}

	@Override
	public void render() {
		// Clear screen first before rendering anything else.
		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// Render everything else.
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
		Core.i().dispose();
	}

	@Override
	@Deprecated
	public void setScreen(Screen screen) {
		// super.setScreen(screen);
		throw new GdxRuntimeException("ScreenManager.setScreen() is deprecated. Instead, use setScreenView()");
	}

	public void setScreenView(ScreenView screen) {
		setScreenView(screen, true);
	}

	public void setScreenView(final ScreenView screen, final boolean listen) {
		// Clear the input listeners for new sceen.
		input.clear();
		if (listen) {
			input.addProcessor(screen);
			input.addProcessor(new GestureDetector(screen));
		}
		// Check if resource has asset waiting to be loaded.
		if (!(screen instanceof LoadScreen) && !AssetManager.i().update()) {
			// Check if load screen is enabled
			if (Config.LOADSCREEN_ENABLED) {
				// Load screen is enabled. So, show load screen.
				try {
					// If no load screen class, set the default.
					if (loadScreenType == null) {
						// Set base default load screen.
						setLoadScreen(LoadScreen.class);
					}
					// Instantiate and set to load screen view.
					LoadScreen loadScreen = (LoadScreen) (loadScreenType.getConstructors()[0]).newInstance(new Object[] {});
					loadScreen.redirectToScreenView(screen);
					setScreenView(loadScreen);
				} catch (Exception e) {
					throw new GdxRuntimeException(e.getMessage());
				}
				return;
			} else {
				// Load screen is disabled. So, block and wait to finish loading all assets.
				AssetManager.i().finishLoading();
				// Continue to set screen.
			}
		}
		// Super setScreen feature:
		// - Disposes current screen automatically.
		// - Resize screen accordingly.
		super.setScreen(screen);
	}

	@Override
	@Deprecated
	public Screen getScreen() {
		return super.getScreen();
	}

	public ScreenView getScreenView() {
		return (ScreenView) super.getScreen();
	}

	public void setBackgroundColor(Color color) {
		backgroundColor = color;
	}

	public InputMultiplexer getInputManager() {
		return input;
	}

	public void setLoadScreen(Class<? extends LoadScreen> type) {
		loadScreenType = type;
	}

}
