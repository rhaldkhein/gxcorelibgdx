package com.gamexplode.core.screen;

import com.badlogic.gdx.Gdx;
import com.gamexplode.core.AssetManager;
import com.gamexplode.core.ConfigCore;
import com.gamexplode.core.Core;

public class LoadScreen extends ScreenView {

	private ScreenView screenView;
	protected AssetManager assetManager;
	
	public LoadScreen() {
		assetManager = AssetManager.i();
	}
	
	public void redirectToScreenView(ScreenView screen) {
		screenView = screen;
	}
	
	@Override
	public void show() {
		super.show();
		Gdx.app.log(ConfigCore.TAG, "LoadScreen | Loading assets...");
	}
	
	@Override
	public void render(float arg0) {
		super.render(arg0);
		if(assetManager.update()){
			// true, done loading.
			Gdx.app.postRunnable(new Runnable() {
				@Override
				public void run() {
					Core.i().getScreenManager().setScreenView(screenView);
				}
			});
		}
	}
	
	public float getProgress() {
		return assetManager.getProgress();
	}

}
