package com.gamexplode.core;

public class Enum {
	public static enum Direction {
		UP, RIGHT, DOWN, LEFT, NONE
	}

	public static enum Position {
		TOP, RIGHT, BOTTOM, LEFT, MIDDLE, CENTER
	}
}
